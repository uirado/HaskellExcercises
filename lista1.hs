-- Calculo das raízes reais de uma função do segundo grau utilizando a formula de bhaskara
-- passando os coeficientes a, b e c, da fórmula a*x^2 + b*x + c, retornando uma tupla das raízes reais (x1, x2)
bhaskara :: Float -> Float -> Float -> (Float, Float)
bhaskara a b c =
    let delta = b*b - 4*a*c
    in  (
    (((-b) + sqrt delta)/(2*a)),
    (((-b) - sqrt delta)/(2*a))
    )

-- QUESTÃO 1: Calculo de area de triangulo a partir de seus lados (Formula de Heron)
isTriangle :: Float -> Float -> Float -> Bool
isTriangle a b c =
    if  a < b+c &&
        b < a+c &&
        c < a+b
    then True
    else False

calculaP :: Float -> Float -> Float -> Float
calculaP a b c = (a + b + c)/2

calculaAreaTriangulo :: Float -> Float -> Float -> Float
calculaAreaTriangulo a b c =
    if isTriangle a b c
        then let p = calculaP a b c in sqrt (p*(p-a)*(p-b)*(p-c))
    else
        error "Os valores não formam um triângulo"

        
-- QUESTÃO 2: Defina um algoritmo que dados a,b,c resolva as raizes do 1º ou 2º grau

-- Funcao do primeiro grau
funcaoPrimeiroGrau :: Float -> Float -> Float
funcaoPrimeiroGrau a b = (-b)/a

-- Calculo de raizes de um polinomio do primeiro ou segundo grau
raizesPolinomio :: Float -> Float -> Float -> ([Char], (Float, Float))
raizesPolinomio a b c
    | a == 0 = ("PrimeiroGrau", (funcaoPrimeiroGrau b c, 0))
    | otherwise = ("SegundoGrau", bhaskara a b c)


-- QUESTÃO 3: Calcular hipotenusa, dado a altura e o ângulo oposto
-- Converter grau para radiano
toRad :: Integer -> Float
toRad a = ((fromInteger a) * pi)/180

calculaHipotenusa :: Float -> Integer -> Float
calculaHipotenusa h a = h / sin (toRad a)


-- QUESTÃO 4: Dado uma lista L, defina uma função que retorne a quantidade de números maiores que N.
-- exemplo: maiores 5 [1,5,12,15] 
-- resposta> 2

maiores :: Int -> [Int] -> Int
maiores n [] = 0
maiores n (x:xs) 
    | n < x = 1 + (maiores n xs)
    | otherwise = maiores n xs