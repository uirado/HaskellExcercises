type Vertice = Int
type Aresta = (Vertice, Vertice)
type Grafo = [Aresta]

grafo :: Grafo
grafo = [(1,2), (1,3), (1,4), (1,5), (2,6), (2,7), (4,8), (5,9)]

adjacente :: Grafo -> Vertice -> [Vertice]
adjacente [] _ = []
adjacente (x:xs) v 
    | (fst x) == v = (snd x) : adjacente xs v
    | otherwise = adjacente xs v
