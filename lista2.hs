type Dia = Int
type Mes = Int
type Ano = Int

---------------------------- QUESTÃO 1 ------------------------------
-- leva em consideração a quantidade de dias de cada mês
-- aceita datas invertidas
qtdDiasMes :: [Int]
qtdDiasMes = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]

intervdatas :: (Dia, Mes) -> (Dia, Mes) -> Int
intervdatas (d1,m1) (d2,m2)
    | m1 == m2 = d2 - d1
    | m1 < m2 = qtdDiasMes !! (m1-1) - d1 + (intervdatas (0,m1+1) (d2,m2))
    | otherwise = intervdatas (d2,m2) (d1,m1)
---------------------------------------------------------------------


---------------------------- QUESTÃO 3 ------------------------------
somaEq :: (Int, Int, Int) -> (Int, Int, Int) -> (Int, Int, Int)
somaEq (a,b,c) (d,e,f) = (a+d,b+e, c+f)
---------------------------------------------------------------------


---------------------------- QUESTÃO 10 -----------------------------
mista :: (Int, Int) -> (Int, Int, Int)
mista (a, b) = (x, y, b)
    where
        x = a `quot` b 
        y = a `mod` b
---------------------------------------------------------------------


---------------------------- QUESTÃO 11 -----------------------------
maiorFr :: (Int, Int, Int) -> (Int, Int, Int) -> (Int, Int, Int)
maiorFr (a,b,c) (x,y,z)
    | a > x = (a,b,c)
    | x > a = (x,y,z)
    | (quot b c) >= (quot y z) && (mod b c) >= (mod y z) = (a,b,c)
    | otherwise = (x,y,z)

menorFr :: (Int, Int, Int) -> (Int, Int, Int) -> (Int, Int, Int)
menorFr (a,b,c) (x,y,z)
    | a > x = (x,y,z)
    | x > a = (a,b,c)
    | (quot b c) >= (quot y z) && (mod b c) >= (mod y z) = (x,y,z)
    | otherwise = (a,b,c)
---------------------------------------------------------------------


--------------------- QUESTÃO 14 (DESAFIO) --------------------------
isDivisivel :: Int -> Int -> Bool
isDivisivel q d
    | q `mod` d == 0 = True
    | otherwise = False

isBissexto :: Int -> Bool
isBissexto a = 
    if a `isDivisivel` 4 then
        if  not (a `isDivisivel` 100) then
            True
        else
            if a `isDivisivel` 400 then
                True
            else 
                False
    else
        False

nomesDias :: [String]
nomesDias = [
    "segunda-feira",
    "terca-feira",
    "quarta-feira",
    "quinta-feira",
    "sexta-feira",
    "sabado",
    "domingo"
    ]

codigoMes :: [Int]
codigoMes = [0, 3, 3, 6, 1, 4, 6, 2, 5, 0, 3, 5]

dianasc :: (Dia, Mes, Ano) -> [Char]
dianasc (dia, mes, ano) = nomesDias !! i
    where 
        anoBase = 1900
        a = ano - 1900
        b = if (isBissexto ano) && (dia <= 29) && (mes <= 2)
            then (a `quot` 4) -1
            else a `quot` 4
        c = codigoMes !! (mes -1)
        d = dia - 1
        i = (a + b + c + d) `mod` 7
---------------------------------------------------------------------