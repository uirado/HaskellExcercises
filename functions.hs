doubleMe x = x + x

doubleUs x y = doubleMe x + doubleMe y

doubleSmallNumber a = if a > 100
                        then a
                        else a*2

-- Função identifica se uma lista é um palindromo
isPalindromo :: (Eq a) => [a] -> Bool
isPalindromo a = reverse a == a

-- Função que identifica o menor inteiro passado
mini :: Int -> Int -> Int
mini a b = if a <= b then a else b  

-- Conta a quantidade de elementos de uma lista qualquer
size :: [a] -> Int
size [] = 0
size (x:xs) = 1 + size xs

-- imprime os elementos de uma lista
-- printEach lista = mapM_ print lista

-- addList a = a ++ lista

fatores :: Int -> [Int]
fatores n = [i | i <- [1..n], n `mod` i == 0]







